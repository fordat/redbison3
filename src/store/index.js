import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentColor: "#e9000b",
    defaultColor: "#e9000b",
  },
  mutations: {
    setCurrentColor(state, color) {
      state.currentColor = color;
    }
  },
  modules: {},
  getters: {
    getCurrentColor(state) {
      return state.currentColor;
    },
    getDefaultColor(state) {
      return state.defaultColor;
    }
  },
});
