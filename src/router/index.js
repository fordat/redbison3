import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Account from "../views/Account.vue";
import Application from "../views/Application.vue";
import ApplicationGeneric from "../views/ApplicationGeneric.vue";
import Configure from "../views/Configure.vue";
import CustomerCare from "../views/CustomerCare/CustomerCare.vue";
import Products from "../views/Products.vue"
import Properties from "../views/Properties.vue"
import Settings from "../views/Settings.vue";
import Users from "../views/Users.vue";

import AddNew from "../views/CustomerCare/AddNewCustomer.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/account",
    name: "Account",
    component: Account,
  },
  {
    path: "/settings",
    name: "settings",
    component: Settings,
  },
  {
    path: "/application",
    name: "Application",
    component: Application,
    children: [
      {
        path: 'generic',
        component: ApplicationGeneric,
      },
      {
        path: 'configure',
        component: Configure,
      },
      {
        path: 'customercare',
        component: CustomerCare,
      },
      {
        path: 'addnew',
        component: AddNew,
      },
      {
        path: 'products',
        component: Products,
      },
      {
        path: 'properties',
        component: Properties,
      },
      {
        path: 'users',
        component: Users,
      },
    ]
  },
];

const router = new VueRouter({
  routes,
});

export default router;
